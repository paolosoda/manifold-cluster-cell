# markers_clustering: it clusters a set of 3D points using a transitive relation closure based on kNN algorithm.  
#
#	Description
#	Example of input line: python markers_clustering.py input_file output_file k_value 
#	It clusters the data contained in "input_file" (where each soma is a row), which is
#	produced by automatic cell localization algorithm described in [1].
#	It returns an output file, which name is specified in "output_file" that contains also the column "Subnet ID".
#	This column contains integer values, starting from 1. Value 0 i s used to represent isolated cell.
#   k_value: value of k for kNN search
#	Example of input line: python markers_clustering.py ms.marker ms_output.marker 8 
#
#	Additional required python packages: NumPy (http://www.numpy.org/)
#
#	When using this function please refer to:
# 	L. Silvestri, M. Paciscopi, P. Soda, F. Biamonte, G. Iannello, P. Frasconi, and F. S. Pavone
#	Quantitative neuroanatomy of all Purkinje cells with light sheet microscopy and high-throughput image analysis
#	Submitted to Frontiers in Neuroanatomy, 2015
#
#   [1] Frasconi, P., Silvestri, L., Soda, P., Cortini, R., Pavone, F.S., and Iannello, G. (2014). 
#	Large-scale automated identification of mouse brain cells in confocal light sheet microscopy images. Bioinformatics 30, i587-593.

import os, sys
import numpy as np


def main(in_full_path, out_full_path, k = 8):
    # Main routine

    markers_xyz, markers_original = markers_file_read(in_full_path)
    markers_dist_pos = markers_distance(markers_xyz)
    KD = markers_to_subnets(markers_xyz, markers_dist_pos, k)
    markers_save(KD, markers_original, in_full_path, out_full_path)


def markers_file_read(in_full_path):
    # Imports the csv file specified in 'in_full_path', first splitting its content by lines and commas, then extracting the
    # markers coordinates and finally returning them

    markers_file = open(in_full_path, 'r')

    markers_original = [f.split(',') for f in markers_file.read().split('\n')]
    markers_xyz = np.array([map(int, m) for m in [ml[0:3] for ml in markers_original[1:len(markers_original) - 1]]])

    markers_file.close()

    return markers_xyz, markers_original


def markers_distance(markers_xyz):
    # This function computes the euclidian distance between all markers and it creates two lists. One list contains the distances (one list for each
    # marker), and another one contains the list of respective positions (one list for marker). This function returns these data using a structure

    Nmarkers = len(markers_xyz)
    class dist_pos: distance, position = [0]*Nmarkers, [0]*Nmarkers

    X, Y, Z = [np.tile(markers_xyz[:, c], (Nmarkers, 1)) for c in range(3)]
    D = np.sqrt(np.power(X - np.transpose(X), 2) + np.power(Y - np.transpose(Y), 2) + np.power(Z - np.transpose(Z), 2))

    D_rows = len(D)
    for i in range(D_rows):
        dist_pos.distance[i] = sorted(D[:, i])[1:D_rows]
        dist_pos.position[i] = sorted(range(len(D)), key = lambda k: D[:, i][k])[1:D_rows]

    return dist_pos


def markers_to_subnets(markers_xyz, dist_pos, k):
    # The function nets the markers using a kNN-based algorithm and it returns the results using a structure

    Nmarkers = len(markers_xyz)

    toCheck_net, toCheck_subnet, knn_links = [1]*Nmarkers, [-1]*Nmarkers, [-1]*Nmarkers
    class KD: net, subnets = [markers_xyz, [], dist_pos], [[-1]*Nmarkers]

    while sum(toCheck_net) != -Nmarkers:
        knn_subnet = [-1]*Nmarkers
        if 1 in toCheck_net: toCheck_subnet[toCheck_net.index(1)] = 1

        while sum(toCheck_subnet) != -Nmarkers:
            i = toCheck_subnet.index(1)
            knn_subnet[i], toCheck_net[i], toCheck_subnet[i] = 1, -1, -1

            i_knn = dist_pos.position[i][:k]
            i_knn_match = [j for j in i_knn if not i in np.append(knn_links[j], -1) and i in dist_pos.position[j][:k]]

            knn_links[i] = i_knn_match
            for s in i_knn_match: toCheck_subnet[s] = 1

        if (len(knn_subnet) - knn_subnet.count(-1)) > 1: KD.subnets[0][KD.subnets[0].index(-1)] = knn_subnet

    KD.net[1] = knn_links
    if -1 in KD.subnets[0]: KD.subnets[0] = KD.subnets[0][:KD.subnets[0].index(-1)]

    return KD


def markers_save(KD, original_file, in_full_path, out_full_path):
    # The function creates the '.marker' file specified in 'out_full_path'. It adds to the input file a column
    # specifying for each marker the net where it is located

    to_save_markers, subnets = KD.net[0], KD.subnets[0]
    netted_file = ['']*(len(to_save_markers) + 2)
    netted_file[0] = original_file[0]; netted_file[0].insert(3, 'Subnet ID')
    for i, c in enumerate(to_save_markers):
        subnet_pos = [[n + 1 for n, s in enumerate(subnets) if s[i] == 1] or [0]][0][0]
        netted_file[i + 1] = [str(s) for s in c] + [str(subnet_pos)] + original_file[i + 1][3:]

    in_label = os.path.split(in_full_path)[-1]
    out_label = os.path.split(out_full_path)[-1]
    if in_full_path == out_full_path:
        out_label = in_label.split('.')[0] + '.'.join(['_netted'] + [in_label.split('.')[1]])
        print('WARNING: saving as \'' + out_label + '\' to not overwrite the original.')
    out_full_path = os.path.join(os.path.split(out_full_path)[0], out_label)

    out_file = open(out_full_path, 'w')
    out_file.write('\n'.join([','.join(s) for s in netted_file]))
    out_file.close()


if __name__ == '__main__':
    # Main function called by terminal

    argv_len = len(sys.argv)
    if argv_len == 1:
        raise Exception('expected ''.marker'' full path in input.')

    elif argv_len == 2:
        print('WARNING: using default parameters value: out_full_path = in_full_path, k = 8.')
        main(sys.argv[1], sys.argv[1])

    elif argv_len == 3:
        print('WARNING: using default paraketer value: k = 8.')
        main(sys.argv[1], sys.argv[2])

    else: main(sys.argv[1], sys.argv[2], int(sys.argv[3]))
